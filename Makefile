.PHONY: all clean

CC = gcc
#CFLAGS = -g
CFLAGS =
PROGS = count_char1 count_char2 counting listechaine tconvert transpose

all: $(PROGS)

% : %.c
	$(CC) $(CFLAGS) $< -o $@

clean:
	rm -f $(PROGS)

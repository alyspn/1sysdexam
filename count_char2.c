#include <stdio.h>
#include <string.h>
#include <ctype.h>


int count_char(char* chaine, char caractere, int saute_case) {
    int i, count = 0;
    if (saute_case) {
        caractere = tolower(caractere);
        for (i = 0; i < strlen(chaine); i++) {
            if (tolower(chaine[i]) == caractere) {
                count++;
            }
        }
    } 
else {
        for (i = 0; i < strlen(chaine); i++) {
            if (chaine[i] == caractere) {
                count++;
            }
        }
    }
    return count;
}
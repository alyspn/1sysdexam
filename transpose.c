#include <stdio.h>


void transpose(float M[3][3], float transM[3][3]){
    int i, j;
    for (i = 0; i < 3; i++) {
        for (j = 0; j < 3; j++) {
            transM[i][j] = M[j][i];
        }
    }
}
int main() {
    float M[3][3] = {
        {1, 2, 3}, 
        {4, 5, 6}, 
        {7, 8, 9}
        };
    float transM[3][3];
    int i, j;

    printf("Matrice : ");
    for (i = 0; i < 3; i++) {
        for (j = 0; j < 3; j++) {
            printf("%.2f\t", M[i][j]);
        }
        printf("\n");
    }

    transpose(M, transM);

    printf("transpose : ");
    for (i = 0; i < 3; i++) {
        for (j = 0; j < 3; j++) {
            printf("%.2f\t", transM[i][j]);
        }
        printf("\n");
    }

    return transM;
}
#include <stdio.h>

def celsius2fahrenheit(celsius: float) -> float:
    return (9/5) * celsius + 32

def fahrenheit2celsius(fahrenheit: float) -> float:
    return (fahrenheit - 32) * 5/9

int main() {
    int choice;
    float temperature, result;

    printf("Si vous voulez convertir du Celsius en Fahrenheit tapez 1. Si vous voulez convertir du Fahrenheit en Celsius tapez 2.");
    scanf("%d", &choice);

     if (choix == 1) {
        printf("inserez ici votre temperature en Celsius : ");
        scanf("%f", &temperature);
        result = celsius2fahrenheit(temperature);
        printf("%.2f°C est egale à %.2f°F.\n", temperature, result);
    }
     else (choix == 2) {
        printf("inserez ici votre temperature en Fahrenheit : ");
        scanf("%f", &temperature);
        result = fahrenheit2celsius(temperature);
        printf("%.2f°F est egale à %.2f°C.\n", temperature, result);
    }
}
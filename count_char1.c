#include <stdio.h>
#include <string.h>

int count_char(char* chaine, char caractere) {
    
    int count = 0;
    for (int i = 0; chaine[i] != '\0'; i++) {
        if (chaine[i] == caractere) {
            count++;
        }
    }
    return count;
}